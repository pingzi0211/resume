[![star](https://gitee.com/itsay/resume/badge/star.svg?theme=white)](https://gitee.com/itsay/resume/stargazers)    [![fork](https://gitee.com/itsay/resume/badge/fork.svg?theme=white)](https://gitee.com/itsay/resume/members)

# 黄慧文个人简历

[ http://pingzi0211.gitee.io/resume]( http://pingzi0211.gitee.io/resume)



此简历模板：

- 内容上，来源于我本人真实求职经历以及参加多次网上简历培训总结而成
- 风格上，参考“张大侠-非你莫属简历模板”
欢迎提issue或者star，ღ( ´･ᴗ･` )比心

# 张大侠模板链接

[https://itsay.gitee.io/resume/](https://itsay.gitee.io/resume/)
